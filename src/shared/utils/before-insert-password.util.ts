import { User } from '@prisma/client';
import * as bcrypt from 'bcryptjs';

const saltRounds = 10;

const beforeInsertPassword = (password: User['password']): string => {
  const hashedPassword = bcrypt.hashSync(password, saltRounds);
  return hashedPassword;
};

export default beforeInsertPassword;
