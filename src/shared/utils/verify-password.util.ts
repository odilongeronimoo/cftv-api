import { HttpException, HttpStatus } from '@nestjs/common';
import * as bcrypt from 'bcryptjs';

const verifyPassword = (password: string, hashedPassword: string): boolean => {
  const matchingPassword = bcrypt.compareSync(password, hashedPassword);
  if (!matchingPassword) {
    throw new HttpException('invalid credentials', HttpStatus.UNAUTHORIZED);
  }
  return true;
};

export default verifyPassword;
