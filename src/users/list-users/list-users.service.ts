import { Injectable } from '@nestjs/common';
import { User } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';

@Injectable()
export class ListUsersService {
  constructor(private database: DatabaseService) {}
  async list(
    take?: number,
    skip?: number,
    email?: User['email'],
  ): Promise<User[] | User> {
    let response: User[] | User;
    if (isNaN(skip)) {
      response = await this.database.user.findMany({
        take: take ? take : 20,
        orderBy: { created_at: 'asc' },
      });
    } else {
      response = await this.database.user.findMany({
        skip,
        take: take ? take : 20,
        orderBy: { created_at: 'asc' },
      });
    }
    if (email) {
      response = await this.database.user.findUnique({ where: { email } });
    }
    return response;
  }
}
