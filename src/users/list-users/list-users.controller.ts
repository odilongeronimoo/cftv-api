import { Controller, Get, Query } from '@nestjs/common';
import { Role, User } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { ListUsersService } from './list-users.service';

@Roles(Role.ADMIN, Role.ROOT, Role.USER)
@Controller('users')
export class ListUsersController {
  constructor(private readonly service: ListUsersService) {}
  @Get()
  async list(
    @Query('take') take?: number,
    @Query('skip') skip?: number,
    @Query('email') email?: User['email'],
  ): ReturnType<ListUsersService['list']> {
    const response = await this.service.list(take, skip, email);
    return response;
  }
}
