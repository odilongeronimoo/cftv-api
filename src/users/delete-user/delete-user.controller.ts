import { Controller, Delete, Param } from '@nestjs/common';
import { Role, User } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { DeleteUserService } from './delete-user.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('users')
export class DeleteUserController {
  constructor(private readonly service: DeleteUserService) {}
  @Delete(':id')
  async delete(
    @Param('id') id: User['id'],
  ): ReturnType<DeleteUserService['delete']> {
    const response = await this.service.delete(id);
    return response;
  }
}
