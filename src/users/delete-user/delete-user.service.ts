import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { User } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';

@Injectable()
export class DeleteUserService {
  constructor(private database: DatabaseService) {}
  async delete(id: User['id']): Promise<true> {
    const userExist = (await this.database.user.count({ where: { id } })) > 0;
    const userNotExist = !userExist;
    if (userNotExist) {
      throw new HttpException('user not found', HttpStatus.NOT_FOUND);
    }
    await this.database.user.delete({ where: { id } });
    return true;
  }
}
