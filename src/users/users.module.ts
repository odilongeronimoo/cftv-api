import { Module } from '@nestjs/common';
import { CreateUserController } from './create-user/create-user.controller';
import { CreateUserService } from './create-user/create-user.service';
import { DeleteUserController } from './delete-user/delete-user.controller';
import { DeleteUserService } from './delete-user/delete-user.service';
import { GetUserByEmailController } from './get-user-by-email/get-user-by-email.controller';
import { GetUserByEmailService } from './get-user-by-email/get-user-by-email.service';
import { GetUserController } from './get-user/get-user.controller';
import { GetUserService } from './get-user/get-user.service';
import { ListUsersController } from './list-users/list-users.controller';
import { ListUsersService } from './list-users/list-users.service';
import { UpdateUserController } from './update-user/update-user.controller';
import { UpdateUserService } from './update-user/update-user.service';

@Module({
  controllers: [
    CreateUserController,
    DeleteUserController,
    GetUserController,
    ListUsersController,
    UpdateUserController,
    GetUserByEmailController,
  ],
  providers: [
    GetUserService,
    CreateUserService,
    UpdateUserService,
    DeleteUserService,
    ListUsersService,
    GetUserByEmailService,
  ],
})
export class UserModule {}
