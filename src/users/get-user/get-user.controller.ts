import { Controller, Get, Param } from '@nestjs/common';
import { Role, User } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { GetUserService } from './get-user.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('users')
export class GetUserController {
  constructor(private readonly service: GetUserService) {}
  @Get(':id')
  async get(@Param('id') id: User['id']): ReturnType<GetUserService['get']> {
    const response = await this.service.get(id);
    return response;
  }
}
