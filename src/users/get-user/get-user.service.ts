import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { User } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';

@Injectable()
export class GetUserService {
  constructor(private database: DatabaseService) {}
  async get(id: User['id']): Promise<User> {
    const user = await this.database.user.findFirst({ where: { id } });
    const userNotExist = !user;
    if (userNotExist) {
      throw new HttpException('user not found', HttpStatus.NOT_FOUND);
    }
    return user;
  }
}
