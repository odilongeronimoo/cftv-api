import { Body, Controller, Post } from '@nestjs/common';
import { Role } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { CreateUserDTO } from '../dto/create-user.dto';
import { CreateUserService } from './create-user.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('users')
export class CreateUserController {
  constructor(private readonly service: CreateUserService) {}
  @Post()
  async create(
    @Body() input: CreateUserDTO,
  ): ReturnType<CreateUserService['create']> {
    const response = await this.service.create(input);
    return response;
  }
}
