import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { DatabaseService } from '../../database/database.service';
import beforeInsertPassword from '../../shared/utils/before-insert-password.util';
import { CreateUserDTO } from '../dto/create-user.dto';

@Injectable()
export class CreateUserService {
  constructor(private database: DatabaseService) {}
  async create(input: CreateUserDTO): Promise<void> {
    const emailAlreadyExists =
      (await this.database.user.count({
        where: { email: input.email },
      })) > 0;
    if (emailAlreadyExists) {
      throw new HttpException('email already exists', HttpStatus.BAD_REQUEST);
    }
    await this.database.user.create({
      data: { ...input, password: beforeInsertPassword(input.password) },
    });
  }
}
