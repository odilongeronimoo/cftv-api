import { Controller, Get, Param } from '@nestjs/common';
import { Role, User } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { GetUserByEmailService } from './get-user-by-email.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('users')
export class GetUserByEmailController {
  constructor(private readonly service: GetUserByEmailService) {}
  @Get(':id')
  async get(
    @Param('email') email: User['email'],
  ): ReturnType<GetUserByEmailService['get']> {
    const response = await this.service.get(email);
    return response;
  }
}
