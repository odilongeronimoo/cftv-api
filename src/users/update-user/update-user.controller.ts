import { Body, Controller, Param, Patch } from '@nestjs/common';
import { Role, User } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { UpdateUserDTO } from '../dto/update-user.dto';
import { UpdateUserService } from './update-user.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('users')
export class UpdateUserController {
  constructor(private readonly service: UpdateUserService) {}
  @Patch(':id')
  async update(
    @Param('id') id: User['id'],
    @Body() input: UpdateUserDTO,
  ): ReturnType<UpdateUserService['update']> {
    const response = await this.service.update(id, input);
    return response;
  }
}
