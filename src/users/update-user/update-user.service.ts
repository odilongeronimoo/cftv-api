import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { User } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';
import beforeInsertPassword from '../../shared/utils/before-insert-password.util';
import { UpdateUserDTO } from '../dto/update-user.dto';

@Injectable()
export class UpdateUserService {
  constructor(private database: DatabaseService) {}
  async update(id: User['id'], input: UpdateUserDTO): Promise<User> {
    let password = '';
    const user = await this.database.user.findFirst({ where: { id } });
    const userNotExist = !user;
    if (userNotExist) {
      throw new HttpException('user not found', HttpStatus.NOT_FOUND);
    }
    const emailAlreadyExists =
      (await this.database.user.count({
        where: { id: { not: id }, AND: { email: input.email } },
      })) > 0;
    if (emailAlreadyExists) {
      throw new HttpException('email already exists', HttpStatus.BAD_REQUEST);
    }
    const shouldUpdatePassword = !!input.password;
    if (shouldUpdatePassword) {
      const newHashedPassword = beforeInsertPassword(input.password);
      password = newHashedPassword;
    } else {
      password = user.password;
    }
    return this.database.user.update({
      where: { id },
      data: { ...input, password },
    });
  }
}
