import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import * as request from 'supertest';
import { faker } from '@faker-js/faker';
import { DatabaseService } from '../../database/database.service';
import { UpdateCentralAlarmDTO } from '../dto/update-central-alarm.dto';
import { makeUser, makeUserAccessToken } from '../../user';
import { Role } from '@prisma/client';
import configureApp from '../../configure-app';

describe('Update Central Alarm (e2e)', () => {
  let app: INestApplication;
  let database: DatabaseService;
  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleRef.createNestApplication();
    configureApp(app);
    database = app.get<DatabaseService>(DatabaseService);
    await database.centralAlarm.deleteMany();
    await app.init();
  });

  afterAll(async () => {
    const deleteCentralAlarms = database.centralAlarm.deleteMany();
    await database.$transaction([deleteCentralAlarms]);

    await database.$disconnect();
    await app.close();
  });

  const makeDTO = (
    input?: Partial<UpdateCentralAlarmDTO>,
  ): UpdateCentralAlarmDTO => ({
    type: faker.datatype.string(),
    ...input,
  });

  it('should return UNAUTHORIZED when user is not logged in', async () => {
    const id = faker.datatype.uuid();
    const { body } = await request(app.getHttpServer())
      .patch(`/central-alarms/${id}`)
      .send();
    expect(body.statusCode).toBe(HttpStatus.UNAUTHORIZED);
    expect(body.message).toBe('No auth token');
  });

  it('should return FORBIDDEN when user has no permissions', async () => {
    const { id } = await database.centralAlarm.create({
      data: {
        type: faker.datatype.string(),
        serial_number: faker.datatype.hexadecimal({
          length: 10,
          case: 'upper',
        }),
        data: {
          name: faker.datatype.string(),
          model: faker.datatype.string(),
          firmware_version: faker.system.semver(),
          username: faker.internet.userName(),
          password: faker.internet.password(),
          ipv4: faker.internet.ip(),
          access_mode: faker.datatype.string(),
          download_password: faker.datatype.string(),
          port: faker.datatype.number(),
        },
      },
    });
    const input = makeDTO();
    const user = await makeUser(Role.USER);
    const accessToken = makeUserAccessToken(app, user);
    const { body } = await request(app.getHttpServer())
      .patch(`/central-alarms/${id}`)
      .set('Authorization', `Bearer ${accessToken}`)
      .send({ input });
    expect(body.message).toBe('Forbidden resource');
    expect(body.error).toBe('Forbidden');
    expect(body.statusCode).toBe(HttpStatus.FORBIDDEN);
  });

  it('should return HttpException when update central alarm not found', async () => {
    const id = faker.datatype.uuid();
    const input = makeDTO();
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { body } = await request(app.getHttpServer())
      .patch(`/central-alarms/${id}`)
      .set('Authorization', `Bearer ${accessToken}`)
      .send(input);
    expect(body.statusCode).toBe(HttpStatus.NOT_FOUND);
    expect(body.message).toBe('central alarm not found');
  });

  it('should update central alarm', async () => {
    const { id } = await database.centralAlarm.create({
      data: {
        type: faker.datatype.string(),
        serial_number: faker.datatype.hexadecimal({
          length: 10,
          case: 'upper',
        }),
        data: {
          name: faker.datatype.string(),
          model: faker.datatype.string(),
          firmware_version: faker.system.semver(),
          username: faker.internet.userName(),
          password: faker.internet.password(),
          ipv4: faker.internet.ip(),
          access_mode: faker.datatype.string(),
          download_password: faker.datatype.string(),
          port: faker.datatype.number(),
        },
      },
    });
    const input = makeDTO();
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { status, body } = await request(app.getHttpServer())
      .patch(`/central-alarms/${id}`)
      .set('Authorization', `Bearer ${accessToken}`)
      .send(input);
    const centralAlarmOnDb = await database.centralAlarm.findFirst({
      where: { id: body.id },
    });

    expect(status).toBe(HttpStatus.OK);
    expect(body).toMatchObject({
      ...centralAlarmOnDb,
      created_at: centralAlarmOnDb.created_at.toISOString(),
      updated_at: centralAlarmOnDb.updated_at.toISOString(),
    });
  });
});
