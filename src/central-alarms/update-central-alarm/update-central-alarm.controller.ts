import { Body, Controller, Param, Patch } from '@nestjs/common';
import { CentralAlarm, Role } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { UpdateCentralAlarmDTO } from '../dto/update-central-alarm.dto';
import { UpdateCentralAlarmService } from './update-central-alarm.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('central-alarms')
export class UpdateCentralAlarmController {
  constructor(private readonly service: UpdateCentralAlarmService) {}
  @Patch(':id')
  async update(
    @Param('id') id: CentralAlarm['id'],
    @Body() input: UpdateCentralAlarmDTO,
  ): ReturnType<UpdateCentralAlarmService['update']> {
    const response = await this.service.update(id, input);
    return response;
  }
}
