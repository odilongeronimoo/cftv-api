import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CentralAlarm } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';
import { UpdateCentralAlarmDTO } from '../dto/update-central-alarm.dto';

@Injectable()
export class UpdateCentralAlarmService {
  constructor(private database: DatabaseService) {}
  async update(
    id: CentralAlarm['id'],
    input: UpdateCentralAlarmDTO,
  ): Promise<CentralAlarm> {
    const centralAlarm = await this.database.centralAlarm.findFirst({
      where: { id },
    });
    const centralAlarmNotFound = !centralAlarm;
    if (centralAlarmNotFound) {
      throw new HttpException('central alarm not found', HttpStatus.NOT_FOUND);
    }
    const updatedCentralAlarm = await this.database.centralAlarm.update({
      where: { id },
      data: { ...input },
    });
    return updatedCentralAlarm;
  }
}
