import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CentralAlarm } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';

@Injectable()
export class GetCentralAlarmService {
  constructor(private database: DatabaseService) {}
  async get(id: CentralAlarm['id']): Promise<CentralAlarm> {
    const centralAlarm = await this.database.centralAlarm.findFirst({
      where: { id },
    });
    const centralAlarmNotFound = !centralAlarm;
    if (centralAlarmNotFound) {
      throw new HttpException('central alarm not found', HttpStatus.NOT_FOUND);
    }
    return centralAlarm;
  }
}
