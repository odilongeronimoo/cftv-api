import { Controller, Get, Param } from '@nestjs/common';
import { CentralAlarm, Role } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { GetCentralAlarmService } from './get-central-alarm.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('central-alarms')
export class GetCentralAlarmController {
  constructor(private readonly service: GetCentralAlarmService) {}
  @Get(':id')
  async get(
    @Param('id') id: CentralAlarm['id'],
  ): ReturnType<GetCentralAlarmService['get']> {
    const response = await this.service.get(id);
    return response;
  }
}
