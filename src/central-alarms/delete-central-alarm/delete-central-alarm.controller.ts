import { Controller, Delete, Param } from '@nestjs/common';
import { CentralAlarm, Role } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { DeleteCentralAlarmService } from './delete-central-alarm.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('central-alarms')
export class DeleteCentralAlarmController {
  constructor(private readonly service: DeleteCentralAlarmService) {}
  @Delete(':id')
  async delete(
    @Param('id') id: CentralAlarm['id'],
  ): ReturnType<DeleteCentralAlarmService['delete']> {
    const response = await this.service.delete(id);
    return response;
  }
}
