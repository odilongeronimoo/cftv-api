import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CentralAlarm } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';

@Injectable()
export class DeleteCentralAlarmService {
  constructor(private database: DatabaseService) {}
  async delete(
    id: CentralAlarm['id'],
  ): Promise<{ deleted: true; message?: string }> {
    const centralAlarmExist =
      (await this.database.centralAlarm.count({
        where: { id },
      })) > 0;
    const centralAlarmNotFound = !centralAlarmExist;
    if (centralAlarmNotFound) {
      throw new HttpException('central alarm not found', HttpStatus.NOT_FOUND);
    }
    try {
      await this.database.centralAlarm.delete({ where: { id } });
      return { deleted: true, message: 'Central Alarm deleted' };
    } catch (error) {
      throw new HttpException(
        {
          statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
          message: 'Internal Server Error',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
