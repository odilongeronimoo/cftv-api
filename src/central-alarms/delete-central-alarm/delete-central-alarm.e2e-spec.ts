import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import * as request from 'supertest';
import { faker } from '@faker-js/faker';
import { DatabaseService } from '../../database/database.service';
import { makeUser, makeUserAccessToken } from '../../user';
import { Role } from '@prisma/client';
import configureApp from '../../configure-app';
import { CreateCentralAlarmDTO } from '../dto/create-central-alarm.dto';

describe('Delete Central Alarm (e2e)', () => {
  let app: INestApplication;
  let database: DatabaseService;
  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleRef.createNestApplication();
    configureApp(app);
    database = app.get<DatabaseService>(DatabaseService);
    await database.centralAlarm.deleteMany();

    await app.init();
  });

  afterAll(async () => {
    const deleteCentralAlarms = database.centralAlarm.deleteMany();
    await database.$transaction([deleteCentralAlarms]);

    await database.$disconnect();
    await app.close();
  });

  const makeDTO = (
    input?: Partial<CreateCentralAlarmDTO>,
  ): CreateCentralAlarmDTO => ({
    type: faker.datatype.string(),
    serial_number: faker.datatype.hexadecimal({ length: 10, case: 'upper' }),
    data: {
      name: faker.datatype.string(),
      model: faker.datatype.string(),
      firmware_version: faker.system.semver(),
      username: faker.internet.userName(),
      password: faker.internet.password(),
      ipv4: faker.internet.ip(),
      access_mode: faker.datatype.string(),
      download_password: faker.datatype.string(),
      port: faker.datatype.number(),
    },
    ...input,
  });

  it('should return UNAUTHORIZED when user is not logged in', async () => {
    const input = makeDTO();
    const { body } = await request(app.getHttpServer())
      .post('/central-alarms')
      .send({ input });
    expect(body.statusCode).toBe(HttpStatus.UNAUTHORIZED);
    expect(body.message).toBe('No auth token');
  });

  it('should return FORBIDDEN when user has no permissions', async () => {
    const input = makeDTO();
    const user = await makeUser(Role.USER);
    const accessToken = makeUserAccessToken(app, user);
    const { body } = await request(app.getHttpServer())
      .post('/central-alarms')
      .set('Authorization', `Bearer ${accessToken}`)
      .send({ input });
    expect(body.message).toBe('Forbidden resource');
    expect(body.error).toBe('Forbidden');
    expect(body.statusCode).toBe(HttpStatus.FORBIDDEN);
  });

  it('should return HttpException when central alarm central alarm not found', async () => {
    const id = faker.datatype.uuid();
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { body, statusCode } = await request(app.getHttpServer())
      .delete(`/central-alarms/${id}`)
      .set('Authorization', `Bearer ${accessToken}`);

    const countCentralAlarmAfterDeleteOnDb = await database.centralAlarm.count({
      where: { id },
    });

    expect(countCentralAlarmAfterDeleteOnDb).toBe(0);
    expect(statusCode).toBe(HttpStatus.NOT_FOUND);
    expect(body).toStrictEqual({
      statusCode: HttpStatus.NOT_FOUND,
      message: 'central alarm not found',
    });
  });

  it('should delete central alarm', async () => {
    const input = makeDTO();
    const centralAlarmBeforeDeleteOnDb = await database.centralAlarm.create({
      data: {
        type: input.type,
        serial_number: input.serial_number,
        data: input.data,
      },
    });
    const countCentralAlarmBeforeDeleteOnDb = await database.centralAlarm.count(
      { where: { id: centralAlarmBeforeDeleteOnDb.id } },
    );
    expect(countCentralAlarmBeforeDeleteOnDb).toBe(1);
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { body, statusCode } = await request(app.getHttpServer())
      .delete(`/central-alarms/${centralAlarmBeforeDeleteOnDb.id}`)
      .set('Authorization', `Bearer ${accessToken}`);

    const centralAlarmOnDb = await database.centralAlarm.findFirst({
      where: { id: centralAlarmBeforeDeleteOnDb.id },
    });
    expect(centralAlarmOnDb).toBeNull();
    expect(statusCode).toBe(HttpStatus.OK);
    expect(body).toStrictEqual({
      deleted: true,
      message: 'Central Alarm deleted',
    });
  });
});
