import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import * as request from 'supertest';
import { faker } from '@faker-js/faker';
import { DatabaseService } from '../../database/database.service';
import { makeUser, makeUserAccessToken } from '../../user';
import { Role } from '@prisma/client';
import configureApp from '../../configure-app';
import { CreateCentralAlarmDTO } from '../dto/create-central-alarm.dto';

describe('List Central Alarms (e2e)', () => {
  let app: INestApplication;
  let database: DatabaseService;
  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleRef.createNestApplication();
    configureApp(app);
    database = app.get<DatabaseService>(DatabaseService);
    await database.centralAlarm.deleteMany();
    await app.init();
  });

  afterAll(async () => {
    const deleteCentralAlarms = database.centralAlarm.deleteMany();
    await database.$transaction([deleteCentralAlarms]);

    await database.$disconnect();
    await app.close();
  });

  const makeDTO = (
    input?: Partial<CreateCentralAlarmDTO>,
  ): CreateCentralAlarmDTO => ({
    type: faker.lorem.words(),
    serial_number: faker.datatype.hexadecimal({ length: 10, case: 'upper' }),
    data: {
      name: faker.datatype.string(),
      model: faker.datatype.string(),
      firmware_version: faker.system.semver(),
      username: faker.internet.userName(),
      password: faker.internet.password(),
      ipv4: faker.internet.ip(),
      access_mode: faker.datatype.string(),
      download_password: faker.datatype.string(),
      port: faker.datatype.number(),
    },
    ...input,
  });

  it('should return UNAUTHORIZED when user is not logged in', async () => {
    const input = makeDTO();
    await database.centralAlarm.create({ data: { ...input } });
    const { body } = await request(app.getHttpServer()).get('/central-alarms');
    expect(body.statusCode).toBe(HttpStatus.UNAUTHORIZED);
    expect(body.message).toBe('No auth token');
  });

  it('should list central alarms', async () => {
    const input = makeDTO();
    await database.centralAlarm.create({
      data: { ...input },
    });
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { status, body } = await request(app.getHttpServer())
      .get('/central-alarms')
      .set('Authorization', `Bearer ${accessToken}`)
      .send(input);

    const centralAlarmOnDb = await database.centralAlarm.findFirst();
    const countCentralAlarmOnDb = await database.centralAlarm.count();

    expect(status).toBe(HttpStatus.OK);
    expect(countCentralAlarmOnDb).toBe(1);
    expect(body).toMatchObject([
      {
        ...centralAlarmOnDb,
        created_at: centralAlarmOnDb.created_at.toISOString(),
        updated_at: centralAlarmOnDb.updated_at.toISOString(),
      },
    ]);
  });

  it('should list central alarms by serial_number', async () => {
    const input = makeDTO();
    const centralAlarm = await database.centralAlarm.create({
      data: { ...input },
    });

    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { status, body } = await request(app.getHttpServer())
      .get(`/central-alarms/?serial_number=${centralAlarm.serial_number}`)
      .set('Authorization', `Bearer ${accessToken}`);

    const countCentralAlarmOnDb = await database.centralAlarm.count({
      where: { id: centralAlarm.id },
    });
    const centralAlarmOnDb = await database.centralAlarm.findFirst({
      where: { id: centralAlarm.id },
    });

    expect(status).toBe(HttpStatus.OK);
    expect(countCentralAlarmOnDb).toBe(1);
    expect(body).toMatchObject({
      ...centralAlarmOnDb,
      created_at: centralAlarmOnDb.created_at.toISOString(),
      updated_at: centralAlarmOnDb.updated_at.toISOString(),
    });
  });

  it('should list the first 20 alarm centers', async () => {
    const input = makeDTO();
    const centralAlarms = await database.centralAlarm.createMany({
      data: [
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.lorem.words(),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
      ],
      skipDuplicates: true,
    });
    expect(centralAlarms.count).toEqual(21);
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { status, body } = await request(app.getHttpServer())
      .get(`/central-alarms?take=${20}`)
      .set('Authorization', `Bearer ${accessToken}`);

    const results = await database.centralAlarm.findMany({ take: 20 });

    expect(status).toBe(HttpStatus.OK);
    expect(body.length).toBe(results.length);
  });
});
