import { Injectable } from '@nestjs/common';
import { CentralAlarm } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';

@Injectable()
export class ListCentralAlarmsService {
  constructor(private database: DatabaseService) {}
  async list(
    take?: number,
    skip?: number,
    serial_number?: CentralAlarm['serial_number'],
  ): Promise<CentralAlarm[] | CentralAlarm> {
    let response: CentralAlarm[] | CentralAlarm;
    if (isNaN(skip)) {
      response = await this.database.centralAlarm.findMany({
        take: take ? take : 20,
        orderBy: { created_at: 'asc' },
      });
    } else {
      response = await this.database.centralAlarm.findMany({
        skip,
        take: take ? take : 20,
        orderBy: { created_at: 'asc' },
      });
    }
    if (serial_number) {
      response = await this.database.centralAlarm.findFirst({
        where: { serial_number },
      });
    }
    return response;
  }
}
