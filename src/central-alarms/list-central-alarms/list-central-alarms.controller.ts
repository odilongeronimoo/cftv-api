import { Controller, Get, Query } from '@nestjs/common';
import { CentralAlarm, Role } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { ListCentralAlarmsService } from './list-central-alarms.service';

@Roles(Role.USER, Role.ADMIN, Role.ROOT)
@Controller('central-alarms')
export class ListCentralAlarmsController {
  constructor(private readonly service: ListCentralAlarmsService) {}
  @Get()
  async list(
    @Query('take') take?: number,
    @Query('skip') skip?: number,
    @Query('serial_number') serial_number?: CentralAlarm['serial_number'],
  ): ReturnType<ListCentralAlarmsService['list']> {
    const response = await this.service.list(take, skip, serial_number);
    return response;
  }
}
