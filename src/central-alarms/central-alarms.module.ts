import { Module } from '@nestjs/common';
import { CreateCentralAlarmController } from './create-central-alarm/create-central-alarm.controller';
import { CreateCentralAlarmService } from './create-central-alarm/create-central-alarm.service';
import { DeleteCentralAlarmController } from './delete-central-alarm/delete-central-alarm.controller';
import { DeleteCentralAlarmService } from './delete-central-alarm/delete-central-alarm.service';
import { GetCentralAlarmController } from './get-central-alarm/get-central-alarm.controller';
import { GetCentralAlarmService } from './get-central-alarm/get-central-alarm.service';
import { ListCentralAlarmsController } from './list-central-alarms/list-central-alarms.controller';
import { ListCentralAlarmsService } from './list-central-alarms/list-central-alarms.service';
import { UpdateCentralAlarmController } from './update-central-alarm/update-central-alarm.controller';
import { UpdateCentralAlarmService } from './update-central-alarm/update-central-alarm.service';

@Module({
  controllers: [
    CreateCentralAlarmController,
    DeleteCentralAlarmController,
    GetCentralAlarmController,
    ListCentralAlarmsController,
    UpdateCentralAlarmController,
  ],
  providers: [
    CreateCentralAlarmService,
    DeleteCentralAlarmService,
    GetCentralAlarmService,
    ListCentralAlarmsService,
    UpdateCentralAlarmService,
  ],
})
export class CentralAlarmModule {}
