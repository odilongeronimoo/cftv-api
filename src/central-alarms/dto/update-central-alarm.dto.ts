import { OmitType, PartialType } from '@nestjs/mapped-types';
import { CentralAlarm } from '@prisma/client';
import { IsOptional } from 'class-validator';
import { CreateCentralAlarmDTO } from './create-central-alarm.dto';

export class UpdateCentralAlarmDTO extends PartialType(
  OmitType(CreateCentralAlarmDTO, ['data', 'serial_number'] as const),
) {
  @IsOptional()
  type?: CentralAlarm['type'];
}
