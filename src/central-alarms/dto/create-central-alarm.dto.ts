import {
  IsAlphanumeric,
  IsNotEmpty,
  IsObject,
  IsString,
} from 'class-validator';
import { Prisma } from '@prisma/client';
import { Transform } from 'class-transformer';

export class CreateCentralAlarmDTO {
  @Transform(({ value }) => value.trim())
  @Transform(({ value }) => value.toUpperCase())
  @Transform(({ value }) => value.replaceAll(' ', '_'))
  @IsString({ message: 'type must be string' })
  @IsNotEmpty({ message: 'Type is required' })
  type: string;

  @Transform(({ value }) => value.toUpperCase())
  @IsAlphanumeric()
  @IsNotEmpty({ message: 'serial number is required' })
  serial_number: string;

  @IsObject()
  @IsNotEmpty({ message: 'data is required' })
  data: Prisma.JsonObject;
}
