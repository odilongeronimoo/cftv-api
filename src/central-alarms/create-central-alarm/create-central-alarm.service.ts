import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CentralAlarm } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';
import { CreateCentralAlarmDTO } from '../dto/create-central-alarm.dto';

@Injectable()
export class CreateCentralAlarmService {
  constructor(private database: DatabaseService) {}
  async create(input: CreateCentralAlarmDTO): Promise<CentralAlarm> {
    const serialNumberAlreadyExists =
      (await this.database.centralAlarm.count({
        where: { serial_number: input.serial_number },
      })) > 0;
    if (serialNumberAlreadyExists) {
      throw new HttpException(
        'serial number already exists',
        HttpStatus.BAD_REQUEST,
      );
    }
    const centralAlarmCreated = await this.database.centralAlarm.create({
      data: {
        type: input.type,
        serial_number: input.serial_number,
        data: input.data,
      },
    });
    return this.database.centralAlarm.findUnique({
      where: { id: centralAlarmCreated.id },
    });
  }
}
