import { Body, Controller, Post } from '@nestjs/common';
import { Role } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { CreateCentralAlarmDTO } from '../dto/create-central-alarm.dto';
import { CreateCentralAlarmService } from './create-central-alarm.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('central-alarms')
export class CreateCentralAlarmController {
  constructor(private readonly service: CreateCentralAlarmService) {}
  @Post()
  async create(
    @Body() input: CreateCentralAlarmDTO,
  ): ReturnType<CreateCentralAlarmService['create']> {
    const response = await this.service.create(input);
    return response;
  }
}
