import { User } from '@prisma/client';

export interface TokenPayload {
  sub: User['id'];
  email: User['email'];
  roles: User['roles'];
}
