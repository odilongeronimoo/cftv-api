import { Controller, HttpCode, Post, Req, UseGuards } from '@nestjs/common';
import { LoginService } from './login.service';
import { RequestWithUser } from './dto/request-with-user.dto';
import { Public } from '../../shared/decorators/is-public.decorator';
import { LocalAuthenticationGuard } from '../guards/local-auth.guard';

@Public()
@UseGuards(LocalAuthenticationGuard)
@Controller('login')
export class LoginController {
  constructor(private service: LoginService) {}
  @HttpCode(200)
  @Post()
  async login(@Req() request: RequestWithUser) {
    const { user } = request;
    return this.service.generateToken({
      email: user.email,
      roles: user.roles,
      sub: user.id,
    });
  }
}
