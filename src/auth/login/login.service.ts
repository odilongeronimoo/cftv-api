import { HttpException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { TokenPayload } from './dto/token-payload.dto';
import { GetUserByEmailService } from '../../users/get-user-by-email/get-user-by-email.service';
import verifyPassword from '../../shared/utils/verify-password.util';
import { TokenResponse } from './dto/token-response.dto';

@Injectable()
export class LoginService {
  constructor(
    private service: GetUserByEmailService,
    private jwtService: JwtService,
  ) {}

  async generateToken({
    email,
    roles,
    sub,
  }: TokenPayload): Promise<TokenResponse> {
    const payload: TokenPayload = { email, roles, sub };
    const token = this.jwtService.sign(payload);
    const response: TokenResponse = {
      access_token: token,
      expires_in: 3600,
    };
    return response;
  }

  async validateUser(email: string, plainTextPassword: string) {
    const user = await this.service.get(email);
    const isInvalidPassword = !verifyPassword(plainTextPassword, user.password);
    if (isInvalidPassword) {
      throw new HttpException('Invalid credentials', 401);
    }
    delete user.password;
    return user;
  }
}
