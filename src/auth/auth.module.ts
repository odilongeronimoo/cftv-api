import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { LoginController } from './login/login.controller';
import { GetUserByEmailService } from '../users/get-user-by-email/get-user-by-email.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { GetUserService } from '../users/get-user/get-user.service';
import { JwtStrategy } from './strategies/jwt.strategy';
import { LoginService } from './login/login.service';
import { ProfileController } from './profile/profile.controller';
import { APP_GUARD } from '@nestjs/core';
import { JwtAuthenticationGuard } from './guards/jwt-auth.guard';
import { RolesGuard } from './guards/roles.guard';
import { LocalStrategy } from './strategies/local.strategy';
import { UserModule } from '../users/users.module';

@Module({
  imports: [
    ConfigModule,
    UserModule,
    PassportModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get('JWT_SECRET'),
        signOptions: { expiresIn: 3600 },
      }),
    }),
  ],
  controllers: [LoginController, ProfileController],
  providers: [
    GetUserByEmailService,
    GetUserService,
    LoginService,
    JwtStrategy,
    LocalStrategy,
    {
      provide: APP_GUARD,
      useClass: JwtAuthenticationGuard,
    },
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },
  ],
  exports: [AuthModule],
})
export class AuthModule {}
