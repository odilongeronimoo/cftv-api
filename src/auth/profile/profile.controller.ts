import { Controller, Get, HttpCode, Req } from '@nestjs/common';
import { RequestWithUser } from '../login/dto/request-with-user.dto';

@Controller('profile')
export class ProfileController {
  @HttpCode(200)
  @Get()
  profile(@Req() request: RequestWithUser) {
    const user = request.user;
    delete user.password;
    return user;
  }
}
