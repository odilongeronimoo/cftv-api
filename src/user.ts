import { faker } from '@faker-js/faker';
import { INestApplication } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Role, User, PrismaClient } from '@prisma/client';

export const makeUser = async (...roles: Partial<Role[]>): Promise<User> => {
  const database = new PrismaClient();
  database.$connect();
  const user = database.user.create({
    data: {
      name: faker.internet.userName(),
      email: faker.internet.email(),
      password: faker.internet.password(),
      roles: { set: roles },
    },
  });
  return user;
};

export const makeUserAccessToken = (
  application: INestApplication,
  user: User,
): string => {
  const jwtService = application.get(JwtService);
  return jwtService.sign({
    sub: user.id,
    roles: user.roles,
    email: user.email,
  });
};
