import { Controller, Delete, Param } from '@nestjs/common';
import { Camera, Role } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { DeleteCameraService } from './delete-camera.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('cameras')
export class DeleteCameraController {
  constructor(private readonly service: DeleteCameraService) {}
  @Delete(':id')
  async delete(
    @Param('id') id: Camera['id'],
  ): ReturnType<DeleteCameraService['delete']> {
    const response = await this.service.delete(id);
    return response;
  }
}
