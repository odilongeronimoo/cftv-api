import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Camera } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';

@Injectable()
export class DeleteCameraService {
  constructor(private database: DatabaseService) {}
  async delete(
    id: Camera['id'],
  ): Promise<{ deleted: boolean; message?: string }> {
    const cameraExist =
      (await this.database.camera.count({
        where: { id },
      })) > 0;
    const cameraNotFound = !cameraExist;
    if (cameraNotFound) {
      throw new HttpException('camera not found', HttpStatus.NOT_FOUND);
    }
    try {
      await this.database.camera.delete({ where: { id } });
      return { deleted: true, message: 'camera deleted' };
    } catch (error) {
      throw new HttpException(
        {
          statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
          message: 'Internal Server Error',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
