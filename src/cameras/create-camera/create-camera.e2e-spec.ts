import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import * as request from 'supertest';
import { faker } from '@faker-js/faker';
import { DatabaseService } from '../../database/database.service';
import { CreateCameraDTO } from '../dto/create-camera.dto';
import { makeUser, makeUserAccessToken } from '../../user';
import { CameraType, Role } from '@prisma/client';
import configureApp from '../../configure-app';

describe('Create Camera (e2e)', () => {
  let app: INestApplication;
  let database: DatabaseService;
  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleRef.createNestApplication();
    configureApp(app);
    database = app.get<DatabaseService>(DatabaseService);
    await database.camera.deleteMany();
    await app.init();
  });

  afterAll(async () => {
    const deleteCameras = database.camera.deleteMany();
    await database.$transaction([deleteCameras]);

    await database.$disconnect();
    await app.close();
  });

  const makeDTO = (input?: Partial<CreateCameraDTO>): CreateCameraDTO => ({
    serial_number: faker.datatype.hexadecimal({ case: 'upper', length: 10 }),
    type: faker.helpers.arrayElement(Object.values(CameraType)),
    data: {
      name: faker.datatype.string(),
      model: faker.datatype.string(),
      firmware_version: faker.system.semver(),
      username: faker.internet.userName(),
      password: faker.internet.password(),
      ipv4: faker.internet.ip(),
      access_mode: faker.datatype.string(),
      download_password: faker.datatype.string(),
      port: faker.datatype.number(),
    },
    ...input,
  });

  it('should return UNAUTHORIZED when user is not logged in', async () => {
    const input = makeDTO();
    const { body } = await request(app.getHttpServer())
      .post('/cameras')
      .send({ input });
    expect(body.statusCode).toBe(HttpStatus.UNAUTHORIZED);
    expect(body.message).toBe('No auth token');
  });

  it('should return FORBIDDEN when user has no permissions', async () => {
    const input = makeDTO();
    const user = await makeUser(Role.USER);
    const accessToken = makeUserAccessToken(app, user);
    const { body } = await request(app.getHttpServer())
      .post('/cameras')
      .set('Authorization', `Bearer ${accessToken}`)
      .send({ input });
    expect(body.message).toBe('Forbidden resource');
    expect(body.error).toBe('Forbidden');
    expect(body.statusCode).toBe(HttpStatus.FORBIDDEN);
  });

  it('should return BAD_USER_INPUT when creating camera and serial_number is empty', async () => {
    const input = makeDTO({ serial_number: '' });
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { body } = await request(app.getHttpServer())
      .post('/cameras')
      .set('Authorization', `Bearer ${accessToken}`)
      .send(input);
    expect(body.statusCode).toBe(400);
    expect(body.message).toStrictEqual([
      'serial number is required',
      'serial_number must contain only letters and numbers',
    ]);
  });

  it('should create camera', async () => {
    const input = makeDTO();
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { status, body } = await request(app.getHttpServer())
      .post('/cameras')
      .set('Authorization', `Bearer ${accessToken}`)
      .send(input);
    const cameraOnDb = await database.camera.findFirst({
      where: { id: body.id },
    });
    const countCameraOnDb = await database.camera.count({
      where: { id: cameraOnDb.id },
    });

    expect(status).toBe(HttpStatus.CREATED);
    expect(countCameraOnDb).toBe(1);
    expect(body).toMatchObject({
      ...cameraOnDb,
      created_at: cameraOnDb.created_at.toISOString(),
      updated_at: cameraOnDb.updated_at.toISOString(),
    });
  });
});
