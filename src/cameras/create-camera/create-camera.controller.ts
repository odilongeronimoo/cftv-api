import { Body, Controller, Post } from '@nestjs/common';
import { Role } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { CreateCameraDTO } from '../dto/create-camera.dto';
import { CreateCameraService } from './create-camera.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('cameras')
export class CreateCameraController {
  constructor(private readonly service: CreateCameraService) {}

  @Post()
  async create(
    @Body() input: CreateCameraDTO,
  ): ReturnType<CreateCameraService['create']> {
    const response = await this.service.create(input);
    return response;
  }
}
