import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Camera } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';
import { CreateCameraDTO } from '../dto/create-camera.dto';

@Injectable()
export class CreateCameraService {
  constructor(private database: DatabaseService) {}
  async create(input: CreateCameraDTO): Promise<Camera> {
    const serialNumberAlreadyExists =
      (await this.database.camera.count({
        where: { serial_number: input.serial_number },
      })) > 0;
    if (serialNumberAlreadyExists) {
      throw new HttpException(
        'serial number already exists',
        HttpStatus.BAD_REQUEST,
      );
    }
    const createdCamera = await this.database.camera.create({
      data: { ...input },
    });
    return createdCamera;
  }
}
