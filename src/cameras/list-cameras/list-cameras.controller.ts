import { Controller, Get, Query } from '@nestjs/common';
import { Camera, Role } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { ListCamerasService } from './list-cameras.service';

@Roles(Role.ADMIN, Role.ROOT, Role.USER)
@Controller('cameras')
export class ListCamerasController {
  constructor(private readonly service: ListCamerasService) {}

  @Get()
  async list(
    @Query('take') take?: number,
    @Query('skip') skip?: number,
    @Query('serial_number') serial_number?: Camera['serial_number'],
  ): ReturnType<ListCamerasService['list']> {
    const response = await this.service.list(take, skip, serial_number);
    return response;
  }
}
