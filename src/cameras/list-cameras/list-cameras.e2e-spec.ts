import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import * as request from 'supertest';
import { faker } from '@faker-js/faker';
import { DatabaseService } from '../../database/database.service';
import { makeUser, makeUserAccessToken } from '../../user';
import { CameraType, Role } from '@prisma/client';
import configureApp from '../../configure-app';
import { CreateCameraDTO } from '../dto/create-camera.dto';

describe('List Cameras (e2e)', () => {
  let app: INestApplication;
  let database: DatabaseService;
  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleRef.createNestApplication();
    configureApp(app);
    database = app.get<DatabaseService>(DatabaseService);
    await database.camera.deleteMany();
    await app.init();
  });

  afterAll(async () => {
    const deleteCameras = database.camera.deleteMany();
    await database.$transaction([deleteCameras]);

    await database.$disconnect();
    await app.close();
  });

  const makeDTO = (input?: Partial<CreateCameraDTO>): CreateCameraDTO => ({
    type: faker.helpers.arrayElement(Object.values(CameraType)),
    serial_number: faker.datatype.hexadecimal({ length: 10, case: 'upper' }),
    data: {
      name: faker.datatype.string(),
      model: faker.datatype.string(),
      firmware_version: faker.system.semver(),
      username: faker.internet.userName(),
      password: faker.internet.password(),
      ipv4: faker.internet.ip(),
      access_mode: faker.datatype.string(),
      download_password: faker.datatype.string(),
      port: faker.datatype.number(),
    },
    ...input,
  });

  it('should return UNAUTHORIZED when user is not logged in', async () => {
    const { body } = await request(app.getHttpServer()).get('/cameras');
    expect(body.statusCode).toBe(HttpStatus.UNAUTHORIZED);
    expect(body.message).toBe('No auth token');
  });

  it('should list cameras', async () => {
    const input = makeDTO();
    await database.camera.create({ data: { ...input } });
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { status, body } = await request(app.getHttpServer())
      .get('/cameras')
      .set('Authorization', `Bearer ${accessToken}`);

    const countCameraOnDb = await database.camera.count();
    const cameraOnDb = await database.camera.findFirst();

    expect(status).toBe(HttpStatus.OK);
    expect(countCameraOnDb).toBe(1);
    expect(body).toMatchObject([
      {
        ...cameraOnDb,
        created_at: cameraOnDb.created_at.toISOString(),
        updated_at: cameraOnDb.updated_at.toISOString(),
      },
    ]);
  });

  it('should list camera by serial_number', async () => {
    const input = makeDTO();
    const camera = await database.camera.create({ data: { ...input } });
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { status, body } = await request(app.getHttpServer())
      .get(`/cameras/?serial_number=${camera.serial_number}`)
      .set('Authorization', `Bearer ${accessToken}`);

    const countCameraOnDb = await database.camera.count({
      where: { id: camera.id },
    });
    const cameraOnDb = await database.camera.findFirst({
      where: { id: camera.id },
    });

    expect(status).toBe(HttpStatus.OK);
    expect(countCameraOnDb).toBe(1);
    expect(body).toMatchObject({
      ...cameraOnDb,
      created_at: cameraOnDb.created_at.toISOString(),
      updated_at: cameraOnDb.updated_at.toISOString(),
    });
  });

  it('should list the first 20 cameras', async () => {
    const input = makeDTO();
    const cameras = await database.camera.createMany({
      data: [
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(CameraType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
      ],
      skipDuplicates: true,
    });
    expect(cameras.count).toEqual(21);
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { status, body } = await request(app.getHttpServer())
      .get(`/cameras?take=${20}`)
      .set('Authorization', `Bearer ${accessToken}`);

    const results = await database.camera.findMany({ take: 20 });

    expect(status).toBe(HttpStatus.OK);
    expect(body.length).toBe(results.length);
  });
});
