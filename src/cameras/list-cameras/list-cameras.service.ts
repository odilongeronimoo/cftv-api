import { Injectable } from '@nestjs/common';
import { Camera } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';

@Injectable()
export class ListCamerasService {
  constructor(private database: DatabaseService) {}
  async list(
    take?: number,
    skip?: number,
    serial_number?: Camera['serial_number'],
  ): Promise<Camera[] | Camera> {
    let response: Camera[] | Camera;
    if (isNaN(skip)) {
      response = await this.database.camera.findMany({
        take: take ? take : 20,
        orderBy: { created_at: 'asc' },
      });
    } else {
      response = await this.database.camera.findMany({
        skip,
        take: take ? take : 20,
        orderBy: { created_at: 'asc' },
      });
    }
    if (serial_number) {
      response = await this.database.camera.findUnique({
        where: { serial_number },
      });
    }
    return response;
  }
}
