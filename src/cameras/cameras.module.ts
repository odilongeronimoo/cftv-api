import { Module } from '@nestjs/common';
import { AddCameraToVideoDeviceController } from './add-camera-to-video-device/add-camera-to-video-device.controller';
import { AddCameraToVideoDeviceService } from './add-camera-to-video-device/add-camera-to-video-device.service';
import { CreateCameraController } from './create-camera/create-camera.controller';
import { CreateCameraService } from './create-camera/create-camera.service';
import { DeleteCameraController } from './delete-camera/delete-camera.controller';
import { DeleteCameraService } from './delete-camera/delete-camera.service';
import { GetCameraController } from './get-camera/get-camera.controller';
import { GetCameraService } from './get-camera/get-camera.service';
import { ListCamerasController } from './list-cameras/list-cameras.controller';
import { ListCamerasService } from './list-cameras/list-cameras.service';
import { RemoveCameraFromVideoDeviceController } from './remove-camera-from-video-device/remove-camera-from-video-device.controller';
import { RemoveCameraFromVideoDeviceService } from './remove-camera-from-video-device/remove-camera-from-video-device.service';
import { UpdateCameraController } from './update-camera/update-camera.controller';
import { UpdateCameraService } from './update-camera/update-camera.service';

@Module({
  controllers: [
    CreateCameraController,
    DeleteCameraController,
    GetCameraController,
    ListCamerasController,
    UpdateCameraController,
    AddCameraToVideoDeviceController,
    RemoveCameraFromVideoDeviceController,
  ],
  providers: [
    CreateCameraService,
    UpdateCameraService,
    GetCameraService,
    DeleteCameraService,
    ListCamerasService,
    AddCameraToVideoDeviceService,
    RemoveCameraFromVideoDeviceService,
  ],
})
export class CameraModule {}
