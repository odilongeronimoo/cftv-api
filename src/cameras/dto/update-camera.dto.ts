import { OmitType, PartialType } from '@nestjs/mapped-types';
import { CreateCameraDTO } from './create-camera.dto';

export class UpdateCameraDTO extends PartialType(
  OmitType(CreateCameraDTO, ['data', 'serial_number'] as const),
) {}
