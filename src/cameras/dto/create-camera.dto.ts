import { IsAlphanumeric, IsEnum, IsNotEmpty, IsObject } from 'class-validator';
import { CameraType, Prisma } from '@prisma/client';
import { Transform } from 'class-transformer';

export class CreateCameraDTO {
  @IsEnum(CameraType, { message: 'Invalid device type' })
  @IsNotEmpty({ message: 'Type is required' })
  type: CameraType;

  @Transform(({ value }) => value.toUpperCase())
  @IsAlphanumeric()
  @IsNotEmpty({ message: 'serial number is required' })
  serial_number: string;

  @IsObject()
  @IsNotEmpty({ message: 'data is required' })
  data: Prisma.JsonObject;
}
