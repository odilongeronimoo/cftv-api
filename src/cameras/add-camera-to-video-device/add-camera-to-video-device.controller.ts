import { Body, Controller, Post } from '@nestjs/common';
import { Camera, Role, VideoDevice } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { AddCameraToVideoDeviceService } from './add-camera-to-video-device.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('cameras')
export class AddCameraToVideoDeviceController {
  constructor(private readonly service: AddCameraToVideoDeviceService) {}
  @Post('add')
  async add(
    @Body('camera_id') camera_id: Camera['id'],
    @Body('video_device_id') video_device_id: VideoDevice['id'],
  ): ReturnType<AddCameraToVideoDeviceService['addCameraToVideoDevice']> {
    const response = await this.service.addCameraToVideoDevice(
      camera_id,
      video_device_id,
    );
    return response;
  }
}
