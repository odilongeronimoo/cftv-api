import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Camera, VideoDevice } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';

@Injectable()
export class AddCameraToVideoDeviceService {
  constructor(private database: DatabaseService) {}
  async addCameraToVideoDevice(
    camera_id: Camera['id'],
    video_device_id: VideoDevice['id'],
  ): Promise<{ added: boolean; message?: string }> {
    const relationCameraVideoDeviceExist =
      (await this.database.camera.count({
        where: {
          id: camera_id,
          AND: { video_device: { cameras: { some: { id: camera_id } } } },
        },
      })) > 0;
    if (relationCameraVideoDeviceExist) {
      throw new HttpException(
        'The camera is already associated with a DVR',
        HttpStatus.BAD_REQUEST,
      );
    }
    try {
      await this.database.videoDevice.update({
        where: { id: video_device_id },
        data: { cameras: { connect: { id: camera_id } } },
      });
      return { added: true, message: 'Camera added to video device' };
    } catch (error) {
      throw new HttpException(
        {
          statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
          message: 'Internal Server Error',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
