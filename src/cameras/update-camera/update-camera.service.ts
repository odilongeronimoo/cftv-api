import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Camera } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';
import { UpdateCameraDTO } from '../dto/update-camera.dto';

@Injectable()
export class UpdateCameraService {
  constructor(private database: DatabaseService) {}
  async update(id: Camera['id'], input: UpdateCameraDTO): Promise<Camera> {
    const camera = await this.database.camera.findFirst({
      where: { id },
    });
    const cameraNotFound = !camera;
    if (cameraNotFound) {
      throw new HttpException('camera not found', HttpStatus.NOT_FOUND);
    }
    return await this.database.camera.update({
      where: { id },
      data: { ...input },
    });
  }
}
