import { Body, Controller, Param, Patch } from '@nestjs/common';
import { Camera, Role } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { UpdateCameraDTO } from '../dto/update-camera.dto';
import { UpdateCameraService } from './update-camera.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('cameras')
export class UpdateCameraController {
  constructor(private readonly service: UpdateCameraService) {}
  @Patch(':id')
  async update(
    @Param('id') id: Camera['id'],
    @Body() input: UpdateCameraDTO,
  ): ReturnType<UpdateCameraService['update']> {
    const response = await this.service.update(id, input);
    return response;
  }
}
