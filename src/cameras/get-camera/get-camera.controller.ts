import { Controller, Get, Param } from '@nestjs/common';
import { Camera, Role } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { GetCameraService } from './get-camera.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('cameras')
export class GetCameraController {
  constructor(private readonly service: GetCameraService) {}
  @Get(':id')
  async get(
    @Param('id') id: Camera['id'],
  ): ReturnType<GetCameraService['get']> {
    const response = await this.service.get(id);
    return response;
  }
}
