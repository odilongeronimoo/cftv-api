import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Camera } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';

@Injectable()
export class GetCameraService {
  constructor(private database: DatabaseService) {}

  async get(id: Camera['id']): Promise<Camera> {
    const camera = await this.database.camera.findFirst({
      where: { id },
    });
    const cameraNotFound = !camera;
    if (cameraNotFound) {
      throw new HttpException('camera not found', HttpStatus.NOT_FOUND);
    }
    return camera;
  }
}
