import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Camera, VideoDevice } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';

@Injectable()
export class RemoveCameraFromVideoDeviceService {
  constructor(private database: DatabaseService) {}
  async removeCameraFromVideoDevice(
    camera_id: Camera['id'],
    video_device_id: VideoDevice['id'],
  ): Promise<{ removed: boolean; message?: string }> {
    const relationCameraVideoDeviceExist =
      (await this.database.camera.count({
        where: {
          id: camera_id,
          AND: { video_device: { cameras: { some: { id: camera_id } } } },
        },
      })) > 0;
    const relationCameraVideoDeviceNotExist = !relationCameraVideoDeviceExist;
    if (relationCameraVideoDeviceNotExist) {
      throw new HttpException(
        'Camera is not associated with any DVR',
        HttpStatus.BAD_REQUEST,
      );
    }
    try {
      await this.database.videoDevice.update({
        where: { id: video_device_id },
        data: { cameras: { disconnect: { id: camera_id } } },
      });
      return { removed: true, message: 'Camera removed from video device' };
    } catch (error) {
      throw new HttpException(
        {
          statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
          message: 'Internal Server Error',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
