import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import * as request from 'supertest';
import { faker } from '@faker-js/faker';
import { DatabaseService } from '../../database/database.service';
import { makeUser, makeUserAccessToken } from '../../user';
import { CameraType, DeviceType, Role } from '@prisma/client';
import configureApp from '../../configure-app';

describe('Remove Camera From Video Device (e2e)', () => {
  let app: INestApplication;
  let database: DatabaseService;
  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleRef.createNestApplication();
    configureApp(app);
    database = app.get<DatabaseService>(DatabaseService);
    await database.camera.deleteMany();
    await database.videoDevice.deleteMany();
    await app.init();
  });

  afterAll(async () => {
    const deleteCameras = database.camera.deleteMany();
    const deleteVideoDevices = database.videoDevice.deleteMany();
    await database.$transaction([deleteCameras, deleteVideoDevices]);

    await database.$disconnect();
    await app.close();
  });

  it('should return UNAUTHORIZED when user is not logged in', async () => {
    const { body } = await request(app.getHttpServer())
      .post('/cameras/remove')
      .send();
    expect(body.statusCode).toBe(HttpStatus.UNAUTHORIZED);
    expect(body.message).toBe('No auth token');
  });

  it('should return FORBIDDEN when user has no permissions', async () => {
    const camera = await database.camera.create({
      data: {
        type: faker.helpers.arrayElement(Object.values(CameraType)),
        serial_number: faker.datatype.hexadecimal({
          length: 10,
          case: 'upper',
        }),
        data: {
          name: faker.datatype.string(),
          model: faker.datatype.string(),
          firmware_version: faker.system.semver(),
          username: faker.internet.userName(),
          password: faker.internet.password(),
          ipv4: faker.internet.ip(),
          access_mode: faker.datatype.string(),
          download_password: faker.datatype.string(),
          port: faker.datatype.number(),
        },
      },
    });
    const videoDevice = await database.videoDevice.create({
      data: {
        type: faker.helpers.arrayElement(Object.values(DeviceType)),
        serial_number: faker.datatype.hexadecimal({
          length: 10,
          case: 'upper',
        }),
        data: {
          name: faker.datatype.string(),
          model: faker.datatype.string(),
          firmware_version: faker.system.semver(),
          username: faker.internet.userName(),
          password: faker.internet.password(),
          ipv4: faker.internet.ip(),
          access_mode: faker.datatype.string(),
          download_password: faker.datatype.string(),
          port: faker.datatype.number(),
        },
      },
    });

    const user = await makeUser(Role.USER);
    const accessToken = makeUserAccessToken(app, user);
    const { body } = await request(app.getHttpServer())
      .post('/cameras/remove')
      .set('Authorization', `Bearer ${accessToken}`)
      .send({ camera_id: camera.id, video_device_id: videoDevice.id });
    expect(body.message).toBe('Forbidden resource');
    expect(body.error).toBe('Forbidden');
    expect(body.statusCode).toBe(HttpStatus.FORBIDDEN);
  });

  it('should return HttpException when camera is not associated with a DVR', async () => {
    const camera = await database.camera.create({
      data: {
        type: faker.helpers.arrayElement(Object.values(CameraType)),
        serial_number: faker.datatype.hexadecimal({
          length: 10,
          case: 'upper',
        }),
        data: {
          name: faker.datatype.string(),
          model: faker.datatype.string(),
          firmware_version: faker.system.semver(),
          username: faker.internet.userName(),
          password: faker.internet.password(),
          ipv4: faker.internet.ip(),
          access_mode: faker.datatype.string(),
          download_password: faker.datatype.string(),
          port: faker.datatype.number(),
        },
      },
    });
    const videoDevice = await database.videoDevice.create({
      data: {
        type: faker.helpers.arrayElement(Object.values(DeviceType)),
        serial_number: faker.datatype.hexadecimal({
          length: 10,
          case: 'upper',
        }),
        data: {
          name: faker.datatype.string(),
          model: faker.datatype.string(),
          firmware_version: faker.system.semver(),
          username: faker.internet.userName(),
          password: faker.internet.password(),
          ipv4: faker.internet.ip(),
          access_mode: faker.datatype.string(),
          download_password: faker.datatype.string(),
          port: faker.datatype.number(),
        },
      },
    });
    const user = await makeUser(Role.USER, Role.ADMIN, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { body } = await request(app.getHttpServer())
      .post('/cameras/remove')
      .set('Authorization', `Bearer ${accessToken}`)
      .send({ camera_id: camera.id, video_device_id: videoDevice.id });

    expect(body).toMatchObject({
      statusCode: HttpStatus.BAD_REQUEST,
      message: 'Camera is not associated with any DVR',
    });
  });

  it('should remove camera to video device', async () => {
    const camera = await database.camera.create({
      data: {
        type: faker.helpers.arrayElement(Object.values(CameraType)),
        serial_number: faker.datatype.hexadecimal({
          length: 10,
          case: 'upper',
        }),
        data: {
          name: faker.datatype.string(),
          model: faker.datatype.string(),
          firmware_version: faker.system.semver(),
          username: faker.internet.userName(),
          password: faker.internet.password(),
          ipv4: faker.internet.ip(),
          access_mode: faker.datatype.string(),
          download_password: faker.datatype.string(),
          port: faker.datatype.number(),
        },
      },
    });
    const videoDevice = await database.videoDevice.create({
      data: {
        type: faker.helpers.arrayElement(Object.values(DeviceType)),
        serial_number: faker.datatype.hexadecimal({
          length: 10,
          case: 'upper',
        }),
        data: {
          name: faker.datatype.string(),
          model: faker.datatype.string(),
          firmware_version: faker.system.semver(),
          username: faker.internet.userName(),
          password: faker.internet.password(),
          ipv4: faker.internet.ip(),
          access_mode: faker.datatype.string(),
          download_password: faker.datatype.string(),
          port: faker.datatype.number(),
        },
      },
    });
    await database.videoDevice.update({
      where: { id: videoDevice.id },
      data: { cameras: { connect: { id: camera.id } } },
    });

    const user = await makeUser(Role.USER, Role.ADMIN, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { body, statusCode } = await request(app.getHttpServer())
      .post('/cameras/remove')
      .set('Authorization', `Bearer ${accessToken}`)
      .send({ camera_id: camera.id, video_device_id: videoDevice.id });

    expect(statusCode).toBe(HttpStatus.CREATED);
    expect(body).toMatchObject({
      removed: true,
      message: 'Camera removed from video device',
    });
  });
});
