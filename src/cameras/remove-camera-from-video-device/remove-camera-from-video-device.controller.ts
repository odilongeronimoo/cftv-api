import { Body, Controller, Post } from '@nestjs/common';
import { Camera, Role, VideoDevice } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { RemoveCameraFromVideoDeviceService } from './remove-camera-from-video-device.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('cameras')
export class RemoveCameraFromVideoDeviceController {
  constructor(private readonly service: RemoveCameraFromVideoDeviceService) {}
  @Post('remove')
  async removeCameraFromVideoDevice(
    @Body('camera_id') camera_id: Camera['id'],
    @Body('video_device_id') video_device_id: VideoDevice['id'],
  ): ReturnType<
    RemoveCameraFromVideoDeviceService['removeCameraFromVideoDevice']
  > {
    const response = await this.service.removeCameraFromVideoDevice(
      camera_id,
      video_device_id,
    );
    return response;
  }
}
