import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import * as request from 'supertest';
import { faker } from '@faker-js/faker';
import { DatabaseService } from '../../database/database.service';
import { CreateVideoDeviceDTO } from '../dto/create-video-device.dto';
import { makeUser, makeUserAccessToken } from '../../user';
import { DeviceType, Role } from '@prisma/client';
import configureApp from '../../configure-app';

describe('Create Video Device (e2e)', () => {
  let app: INestApplication;
  let database: DatabaseService;
  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleRef.createNestApplication();
    configureApp(app);
    database = app.get<DatabaseService>(DatabaseService);
    await database.videoDevice.deleteMany();
    await app.init();
  });

  afterAll(async () => {
    const deleteVideoDevices = database.videoDevice.deleteMany();
    await database.$transaction([deleteVideoDevices]);

    await database.$disconnect();
    await app.close();
  });

  const makeDTO = (
    input?: Partial<CreateVideoDeviceDTO>,
  ): CreateVideoDeviceDTO => ({
    serial_number: faker.datatype.hexadecimal({ case: 'upper', length: 10 }),
    type: faker.helpers.arrayElement(Object.values(DeviceType)),
    data: {
      name: faker.datatype.string(),
      model: faker.datatype.string(),
      firmware_version: faker.system.semver(),
      username: faker.internet.userName(),
      password: faker.internet.password(),
      ipv4: faker.internet.ip(),
      access_mode: faker.datatype.string(),
      download_password: faker.datatype.string(),
      port: faker.datatype.number(),
    },
    ...input,
  });

  it('should return UNAUTHORIZED when user is not logged in', async () => {
    const input = makeDTO();
    const { body } = await request(app.getHttpServer())
      .post('/video-devices')
      .send({ input });
    expect(body.statusCode).toBe(HttpStatus.UNAUTHORIZED);
    expect(body.message).toBe('No auth token');
  });

  it('should return FORBIDDEN when user has no permissions', async () => {
    const input = makeDTO();
    const user = await makeUser(Role.USER);
    const accessToken = makeUserAccessToken(app, user);
    const { body } = await request(app.getHttpServer())
      .post('/video-devices')
      .set('Authorization', `Bearer ${accessToken}`)
      .send({ input });
    expect(body.message).toBe('Forbidden resource');
    expect(body.error).toBe('Forbidden');
    expect(body.statusCode).toBe(HttpStatus.FORBIDDEN);
  });

  it('should return BAD_USER_INPUT when creating video device and serial_number is empty', async () => {
    const input = makeDTO({ serial_number: '' });
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { body } = await request(app.getHttpServer())
      .post('/video-devices')
      .set('Authorization', `Bearer ${accessToken}`)
      .send(input);
    expect(body.statusCode).toBe(400);
    expect(body.message).toStrictEqual([
      'serial number is required',
      'serial_number must contain only letters and numbers',
    ]);
  });

  it('should create video device', async () => {
    const input = makeDTO();
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { status, body } = await request(app.getHttpServer())
      .post('/video-devices')
      .set('Authorization', `Bearer ${accessToken}`)
      .send(input);
    const videoDeviceOnDb = await database.videoDevice.findFirst();
    const countVideoDeviceOnDb = await database.videoDevice.count({
      where: { id: videoDeviceOnDb.id },
    });

    expect(status).toBe(HttpStatus.CREATED);
    expect(countVideoDeviceOnDb).toBe(1);
    expect(body).toMatchObject({
      ...videoDeviceOnDb,
      created_at: videoDeviceOnDb.created_at.toISOString(),
      updated_at: videoDeviceOnDb.updated_at.toISOString(),
    });
  });
});
