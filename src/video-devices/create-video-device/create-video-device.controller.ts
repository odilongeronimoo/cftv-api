import { Body, Controller, Post } from '@nestjs/common';
import { Role } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { CreateVideoDeviceDTO } from '../dto/create-video-device.dto';
import { CreateVideoDeviceService } from './create-video-device.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('video-devices')
export class CreateVideoDeviceController {
  constructor(private readonly service: CreateVideoDeviceService) {}

  @Post()
  async create(
    @Body() input: CreateVideoDeviceDTO,
  ): ReturnType<CreateVideoDeviceService['create']> {
    const response = await this.service.create(input);
    return response;
  }
}
