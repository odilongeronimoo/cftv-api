import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { VideoDevice } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';
import { CreateVideoDeviceDTO } from '../dto/create-video-device.dto';

@Injectable()
export class CreateVideoDeviceService {
  constructor(private database: DatabaseService) {}
  async create(input: CreateVideoDeviceDTO): Promise<VideoDevice> {
    const serialNumberAlreadyExists =
      (await this.database.videoDevice.count({
        where: { serial_number: input.serial_number },
      })) > 0;
    if (serialNumberAlreadyExists) {
      throw new HttpException(
        'serial number already exists',
        HttpStatus.BAD_REQUEST,
      );
    }
    const createdVideoDevice = await this.database.videoDevice.create({
      data: { ...input },
    });

    return createdVideoDevice;
  }
}
