import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { VideoDevice } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';

@Injectable()
export class GetVideoDeviceService {
  constructor(private database: DatabaseService) {}
  async get(id: VideoDevice['id']): Promise<VideoDevice> {
    const videoDevice = await this.database.videoDevice.findFirst({
      where: { id },
    });
    const videoDeviceNotFound = !videoDevice;
    if (videoDeviceNotFound) {
      throw new HttpException('video device not found', HttpStatus.NOT_FOUND);
    }
    return videoDevice;
  }
}
