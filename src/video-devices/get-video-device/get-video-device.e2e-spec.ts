import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import * as request from 'supertest';
import { faker } from '@faker-js/faker';
import { DatabaseService } from '../../database/database.service';
import { makeUser, makeUserAccessToken } from '../../user';
import { DeviceType, Role } from '@prisma/client';
import configureApp from '../../configure-app';
import { CreateVideoDeviceDTO } from '../dto/create-video-device.dto';

describe('Get Video Device (e2e)', () => {
  let app: INestApplication;
  let database: DatabaseService;
  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleRef.createNestApplication();
    configureApp(app);
    database = app.get<DatabaseService>(DatabaseService);
    await database.videoDevice.deleteMany();
    await app.init();
  });

  afterAll(async () => {
    const deleteVideoDevices = database.videoDevice.deleteMany();
    await database.$transaction([deleteVideoDevices]);

    await database.$disconnect();
    await app.close();
  });

  const makeDTO = (
    input?: Partial<CreateVideoDeviceDTO>,
  ): CreateVideoDeviceDTO => ({
    type: faker.helpers.arrayElement(Object.values(DeviceType)),
    serial_number: faker.datatype.hexadecimal({ length: 10, case: 'upper' }),
    data: {
      name: faker.datatype.string(),
      model: faker.datatype.string(),
      firmware_version: faker.system.semver(),
      username: faker.internet.userName(),
      password: faker.internet.password(),
      ipv4: faker.internet.ip(),
      access_mode: faker.datatype.string(),
      download_password: faker.datatype.string(),
      port: faker.datatype.number(),
    },
    ...input,
  });

  it('should return UNAUTHORIZED when user is not logged in', async () => {
    const input = makeDTO();
    const { id } = await database.videoDevice.create({ data: { ...input } });
    const { body } = await request(app.getHttpServer()).get(
      `/video-devices/${id}`,
    );
    expect(body.statusCode).toBe(HttpStatus.UNAUTHORIZED);
    expect(body.message).toBe('No auth token');
  });

  it('should return FORBIDDEN when user has no permissions', async () => {
    const input = makeDTO();
    const { id } = await database.videoDevice.create({ data: { ...input } });
    const user = await makeUser(Role.USER);
    const accessToken = makeUserAccessToken(app, user);
    const { body } = await request(app.getHttpServer())
      .get(`/video-devices/${id}`)
      .set('Authorization', `Bearer ${accessToken}`);
    expect(body.message).toBe('Forbidden resource');
    expect(body.error).toBe('Forbidden');
    expect(body.statusCode).toBe(HttpStatus.FORBIDDEN);
  });

  it('should get video device', async () => {
    const input = makeDTO();
    const { id } = await database.videoDevice.create({
      data: { ...input },
    });
    const countVideoDeviceBeforeDeleteOnDb = await database.videoDevice.count({
      where: { id },
    });
    expect(countVideoDeviceBeforeDeleteOnDb).toBe(1);
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { status, body } = await request(app.getHttpServer())
      .get(`/video-devices/${id}`)
      .set('Authorization', `Bearer ${accessToken}`);

    const videoDeviceOnDb = await database.videoDevice.findFirst({
      where: { id },
    });
    const countVideoDeviceOnDb = await database.videoDevice.count({
      where: { id },
    });
    expect(status).toBe(HttpStatus.OK);
    expect(countVideoDeviceOnDb).toBe(1);
    expect(body).toMatchObject({
      ...videoDeviceOnDb,
      created_at: videoDeviceOnDb.created_at.toISOString(),
      updated_at: videoDeviceOnDb.updated_at.toISOString(),
    });
  });
});
