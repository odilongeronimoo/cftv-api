import { Controller, Get, Param } from '@nestjs/common';
import { Role, VideoDevice } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { GetVideoDeviceService } from './get-video-device.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('video-devices')
export class GetVideoDeviceController {
  constructor(private readonly service: GetVideoDeviceService) {}
  @Get(':id')
  async get(
    @Param('id') id: VideoDevice['id'],
  ): ReturnType<GetVideoDeviceService['get']> {
    const response = await this.service.get(id);
    return response;
  }
}
