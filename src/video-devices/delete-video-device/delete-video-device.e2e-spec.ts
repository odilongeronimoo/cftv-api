import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import * as request from 'supertest';
import { faker } from '@faker-js/faker';
import { DatabaseService } from '../../database/database.service';
import { makeUser, makeUserAccessToken } from '../../user';
import { DeviceType, Role } from '@prisma/client';
import configureApp from '../../configure-app';
import { CreateVideoDeviceDTO } from '../dto/create-video-device.dto';

describe('Delete Video Device (e2e)', () => {
  let app: INestApplication;
  let database: DatabaseService;
  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleRef.createNestApplication();
    configureApp(app);
    database = app.get<DatabaseService>(DatabaseService);
    await database.videoDevice.deleteMany();

    await app.init();
  });

  afterAll(async () => {
    const deleteVideoDevices = database.videoDevice.deleteMany();
    await database.$transaction([deleteVideoDevices]);

    await database.$disconnect();
    await app.close();
  });

  const makeDTO = (
    input?: Partial<CreateVideoDeviceDTO>,
  ): CreateVideoDeviceDTO => ({
    type: faker.helpers.arrayElement(Object.values(DeviceType)),
    serial_number: faker.datatype.hexadecimal({ length: 10, case: 'upper' }),
    data: {
      name: faker.datatype.string(),
      model: faker.datatype.string(),
      firmware_version: faker.system.semver(),
      username: faker.internet.userName(),
      password: faker.internet.password(),
      ipv4: faker.internet.ip(),
      access_mode: faker.datatype.string(),
      download_password: faker.datatype.string(),
      port: faker.datatype.number(),
    },
    ...input,
  });

  it('should return UNAUTHORIZED when user is not logged in', async () => {
    const id = faker.datatype.uuid();
    const input = makeDTO();
    const { body } = await request(app.getHttpServer())
      .delete(`/video-devices/${id}`)
      .send(input);
    expect(body.statusCode).toBe(HttpStatus.UNAUTHORIZED);
    expect(body.message).toBe('No auth token');
  });

  it('should return FORBIDDEN when user has no permissions', async () => {
    const id = faker.datatype.uuid();
    const input = makeDTO();
    const user = await makeUser(Role.USER);
    const accessToken = makeUserAccessToken(app, user);
    const { body } = await request(app.getHttpServer())
      .delete(`/video-devices/${id}`)
      .set('Authorization', `Bearer ${accessToken}`)
      .send(input);
    expect(body.message).toBe('Forbidden resource');
    expect(body.error).toBe('Forbidden');
    expect(body.statusCode).toBe(HttpStatus.FORBIDDEN);
  });

  it('should return HttpException when video device not found', async () => {
    const id = faker.datatype.uuid();
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { body, statusCode } = await request(app.getHttpServer())
      .delete(`/video-devices/${id}`)
      .set('Authorization', `Bearer ${accessToken}`);

    const countVideoDeviceAfterDeleteOnDb = await database.videoDevice.count({
      where: { id },
    });

    expect(countVideoDeviceAfterDeleteOnDb).toBe(0);
    expect(statusCode).toBe(HttpStatus.NOT_FOUND);
    expect(body).toStrictEqual({
      statusCode: HttpStatus.NOT_FOUND,
      message: 'video device not found',
    });
  });

  it('should delete video device', async () => {
    const input = makeDTO();
    const videoDeviceBeforeDeleteOnDb = await database.videoDevice.create({
      data: {
        type: input.type,
        serial_number: input.serial_number,
        data: input.data,
      },
    });
    const countVideoDeviceBeforeDeleteOnDb = await database.videoDevice.count({
      where: { id: videoDeviceBeforeDeleteOnDb.id },
    });
    expect(countVideoDeviceBeforeDeleteOnDb).toBe(1);
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { body, statusCode } = await request(app.getHttpServer())
      .delete(`/video-devices/${videoDeviceBeforeDeleteOnDb.id}`)
      .set('Authorization', `Bearer ${accessToken}`);

    const videoDeviceOnDb = await database.videoDevice.findFirst({
      where: { id: videoDeviceBeforeDeleteOnDb.id },
    });
    expect(videoDeviceOnDb).toBeNull();
    expect(statusCode).toBe(HttpStatus.OK);
    expect(body).toStrictEqual({
      deleted: true,
      message: 'video device deleted',
    });
  });
});
