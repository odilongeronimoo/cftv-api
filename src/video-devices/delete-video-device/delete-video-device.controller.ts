import { Controller, Delete, Param } from '@nestjs/common';
import { Role, VideoDevice } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { DeleteVideoDeviceService } from './delete-video-device.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('video-devices')
export class DeleteVideoDeviceController {
  constructor(private readonly service: DeleteVideoDeviceService) {}
  @Delete(':id')
  async delete(
    @Param('id') id: VideoDevice['id'],
  ): ReturnType<DeleteVideoDeviceService['delete']> {
    const response = await this.service.delete(id);
    return response;
  }
}
