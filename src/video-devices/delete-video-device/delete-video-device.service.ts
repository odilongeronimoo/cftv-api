import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { VideoDevice } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';

@Injectable()
export class DeleteVideoDeviceService {
  constructor(private database: DatabaseService) {}
  async delete(
    id: VideoDevice['id'],
  ): Promise<{ deleted: boolean; message?: string }> {
    const videoDeviceExist =
      (await this.database.videoDevice.count({ where: { id } })) > 0;
    const videoDeviceNotFound = !videoDeviceExist;
    if (videoDeviceNotFound) {
      throw new HttpException('video device not found', HttpStatus.NOT_FOUND);
    }
    try {
      await this.database.videoDevice.delete({ where: { id } });
      return { deleted: true, message: 'video device deleted' };
    } catch (error) {
      throw new HttpException(
        {
          statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
          message: 'Internal Server Error',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
