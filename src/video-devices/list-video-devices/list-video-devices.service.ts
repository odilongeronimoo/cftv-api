import { Injectable } from '@nestjs/common';
import { VideoDevice } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';

@Injectable()
export class ListVideoDevicesService {
  constructor(private database: DatabaseService) {}
  async list(
    take?: number,
    skip?: number,
    serial_number?: VideoDevice['serial_number'],
  ): Promise<VideoDevice[] | VideoDevice> {
    let response: VideoDevice[] | VideoDevice;
    if (isNaN(skip)) {
      response = await this.database.videoDevice.findMany({
        take: take ? take : 20,
        include: { cameras: { select: { id: true } } },
        orderBy: { created_at: 'asc' },
      });
    } else {
      response = await this.database.videoDevice.findMany({
        skip,
        take: take ? take : 20,
        include: { cameras: { select: { id: true } } },
        orderBy: { created_at: 'asc' },
      });
    }
    if (serial_number) {
      response = await this.database.videoDevice.findUnique({
        where: { serial_number },
        include: { cameras: { select: { id: true } } },
      });
    }
    return response;
  }
}
