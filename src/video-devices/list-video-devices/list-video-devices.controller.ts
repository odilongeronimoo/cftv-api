import { Controller, Get, Query } from '@nestjs/common';
import { Role, VideoDevice } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { ListVideoDevicesService } from './list-video-devices.service';

@Roles(Role.ADMIN, Role.ROOT, Role.USER)
@Controller('video-devices')
export class ListVideoDevicesController {
  constructor(private readonly service: ListVideoDevicesService) {}
  @Get()
  async list(
    @Query('take') take?: number,
    @Query('skip') skip?: number,
    @Query('serial_number') serial_number?: VideoDevice['serial_number'],
  ): ReturnType<ListVideoDevicesService['list']> {
    const response = await this.service.list(take, skip, serial_number);
    return response;
  }
}
