import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import * as request from 'supertest';
import { faker } from '@faker-js/faker';
import { DatabaseService } from '../../database/database.service';
import { makeUser, makeUserAccessToken } from '../../user';
import { DeviceType, Role } from '@prisma/client';
import configureApp from '../../configure-app';
import { CreateVideoDeviceDTO } from '../dto/create-video-device.dto';

describe('List Video Devices (e2e)', () => {
  let app: INestApplication;
  let database: DatabaseService;
  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleRef.createNestApplication();
    configureApp(app);
    database = app.get<DatabaseService>(DatabaseService);
    await database.videoDevice.deleteMany();
    await app.init();
  });

  afterAll(async () => {
    const deleteVideoDevices = database.videoDevice.deleteMany();
    await database.$transaction([deleteVideoDevices]);

    await database.$disconnect();
    await app.close();
  });

  const makeDTO = (
    input?: Partial<CreateVideoDeviceDTO>,
  ): CreateVideoDeviceDTO => ({
    type: faker.helpers.arrayElement(Object.values(DeviceType)),
    serial_number: faker.datatype.hexadecimal({ length: 10, case: 'upper' }),
    data: {
      name: faker.datatype.string(),
      model: faker.datatype.string(),
      firmware_version: faker.system.semver(),
      username: faker.internet.userName(),
      password: faker.internet.password(),
      ipv4: faker.internet.ip(),
      access_mode: faker.datatype.string(),
      download_password: faker.datatype.string(),
      port: faker.datatype.number(),
    },
    ...input,
  });

  it('should return UNAUTHORIZED when user is not logged in', async () => {
    const { body } = await request(app.getHttpServer()).get('/video-devices');
    expect(body.statusCode).toBe(HttpStatus.UNAUTHORIZED);
    expect(body.message).toBe('No auth token');
  });

  it('should list video devices', async () => {
    const input = makeDTO();
    await database.videoDevice.create({
      data: { ...input },
    });
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { status, body } = await request(app.getHttpServer())
      .get('/video-devices')
      .set('Authorization', `Bearer ${accessToken}`);

    const videoDeviceOnDb = await database.videoDevice.findFirst();
    const countVideoDeviceOnDb = await database.videoDevice.count();

    expect(status).toBe(HttpStatus.OK);
    expect(countVideoDeviceOnDb).toBe(1);
    expect(body).toMatchObject([
      {
        ...videoDeviceOnDb,
        created_at: videoDeviceOnDb.created_at.toISOString(),
        updated_at: videoDeviceOnDb.updated_at.toISOString(),
      },
    ]);
  });

  it('should list video device by serial_number', async () => {
    const input = makeDTO();
    const videoDevice = await database.videoDevice.create({
      data: { ...input },
    });

    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { status, body } = await request(app.getHttpServer())
      .get(`/video-devices/?serial_number=${videoDevice.serial_number}`)
      .set('Authorization', `Bearer ${accessToken}`);

    const countVideoDeviceOnDb = await database.videoDevice.count({
      where: { id: videoDevice.id },
    });
    const videoDeviceOnDb = await database.videoDevice.findFirst({
      where: { id: videoDevice.id },
    });

    expect(status).toBe(HttpStatus.OK);
    expect(countVideoDeviceOnDb).toBe(1);
    expect(body).toMatchObject({
      ...videoDeviceOnDb,
      created_at: videoDeviceOnDb.created_at.toISOString(),
      updated_at: videoDeviceOnDb.updated_at.toISOString(),
    });
  });

  it('should list the first 20 video devicers', async () => {
    const input = makeDTO();
    const videoDevices = await database.videoDevice.createMany({
      data: [
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
        {
          ...input,
          serial_number: faker.datatype.hexadecimal({
            length: 10,
            case: 'upper',
          }),
          type: faker.helpers.arrayElement(Object.values(DeviceType)),
          data: {
            name: faker.datatype.string(),
            model: faker.datatype.string(),
            firmware_version: faker.system.semver(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            ipv4: faker.internet.ip(),
            access_mode: faker.datatype.string(),
            download_password: faker.datatype.string(),
            port: faker.datatype.number(),
          },
        },
      ],
      skipDuplicates: true,
    });
    expect(videoDevices.count).toEqual(21);
    const user = await makeUser(Role.ADMIN, Role.USER, Role.ROOT);
    const accessToken = makeUserAccessToken(app, user);
    const { status, body } = await request(app.getHttpServer())
      .get(`/video-devices?take=${20}`)
      .set('Authorization', `Bearer ${accessToken}`);

    const results = await database.videoDevice.findMany({ take: 20 });

    expect(status).toBe(HttpStatus.OK);
    expect(body.length).toBe(results.length);
  });
});
