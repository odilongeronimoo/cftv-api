import { Module } from '@nestjs/common';
import { CreateVideoDeviceController } from './create-video-device/create-video-device.controller';
import { CreateVideoDeviceService } from './create-video-device/create-video-device.service';
import { DeleteVideoDeviceController } from './delete-video-device/delete-video-device.controller';
import { DeleteVideoDeviceService } from './delete-video-device/delete-video-device.service';
import { GetVideoDeviceController } from './get-video-device/get-video-device.controller';
import { GetVideoDeviceService } from './get-video-device/get-video-device.service';
import { ListVideoDevicesController } from './list-video-devices/list-video-devices.controller';
import { ListVideoDevicesService } from './list-video-devices/list-video-devices.service';
import { UpdateVideoDeviceController } from './update-video-device/update-video-device.controller';
import { UpdateVideoDeviceService } from './update-video-device/update-video-device.service';

@Module({
  controllers: [
    CreateVideoDeviceController,
    ListVideoDevicesController,
    UpdateVideoDeviceController,
    DeleteVideoDeviceController,
    GetVideoDeviceController,
  ],
  providers: [
    CreateVideoDeviceService,
    DeleteVideoDeviceService,
    GetVideoDeviceService,
    ListVideoDevicesService,
    UpdateVideoDeviceService,
  ],
})
export class VideoDeviceModule {}
