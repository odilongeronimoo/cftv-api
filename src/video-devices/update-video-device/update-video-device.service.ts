import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { VideoDevice } from '@prisma/client';
import { DatabaseService } from '../../database/database.service';
import { UpdateVideoDeviceDTO } from '../dto/update-video-device.dto';

@Injectable()
export class UpdateVideoDeviceService {
  constructor(private database: DatabaseService) {}
  async update(
    id: VideoDevice['id'],
    input: UpdateVideoDeviceDTO,
  ): Promise<VideoDevice> {
    const videoDevice =
      (await this.database.videoDevice.count({
        where: { id },
      })) > 0;
    const videoDeviceNotFound = !videoDevice;
    if (videoDeviceNotFound) {
      throw new HttpException('video device not found', HttpStatus.NOT_FOUND);
    }
    return this.database.videoDevice.update({
      where: { id },
      data: { ...input },
    });
  }
}
