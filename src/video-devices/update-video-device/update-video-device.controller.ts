import { Body, Controller, Param, Patch } from '@nestjs/common';
import { Role, VideoDevice } from '@prisma/client';
import { Roles } from '../../shared/decorators/roles.decorator';
import { UpdateVideoDeviceDTO } from '../dto/update-video-device.dto';
import { UpdateVideoDeviceService } from './update-video-device.service';

@Roles(Role.ADMIN, Role.ROOT)
@Controller('video-devices')
export class UpdateVideoDeviceController {
  constructor(private readonly service: UpdateVideoDeviceService) {}
  @Patch(':id')
  async update(
    @Param('id') id: VideoDevice['id'],
    @Body() input: UpdateVideoDeviceDTO,
  ): ReturnType<UpdateVideoDeviceService['update']> {
    const response = await this.service.update(id, input);
    return response;
  }
}
