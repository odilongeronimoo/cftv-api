import {
  IsAlphanumeric,
  IsEnum,
  IsNotEmpty,
  IsObject,
  Length,
} from 'class-validator';
import { DeviceType, Prisma } from '@prisma/client';
import { Transform } from 'class-transformer';

export class CreateVideoDeviceDTO {
  @IsEnum(DeviceType, { message: 'Invalid device type' })
  @IsNotEmpty({ message: 'Type is required' })
  type: DeviceType;

  @Transform(({ value }) => value.toUpperCase())
  @IsAlphanumeric()
  @IsNotEmpty({ message: 'serial number is required' })
  serial_number: string;

  @IsObject()
  @IsNotEmpty({ message: 'data is required' })
  data: Prisma.JsonObject;
}
