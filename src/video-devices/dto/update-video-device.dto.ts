import { OmitType, PartialType } from '@nestjs/mapped-types';
import { CreateVideoDeviceDTO } from './create-video-device.dto';

export class UpdateVideoDeviceDTO extends PartialType(
  OmitType(CreateVideoDeviceDTO, ['data', 'serial_number'] as const),
) {}
