import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { VideoDeviceModule } from './video-devices/video-devices.module';
import { CameraModule } from './cameras/cameras.module';
import { CentralAlarmModule } from './central-alarms/central-alarms.module';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './users/users.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    DatabaseModule,
    VideoDeviceModule,
    CameraModule,
    CentralAlarmModule,
    AuthModule,
    UserModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
  ],
})
export class AppModule {}
