-- CreateEnum
CREATE TYPE "DeviceType" AS ENUM ('DVR', 'NVR', 'HVR');

-- CreateEnum
CREATE TYPE "CameraType" AS ENUM ('PINHOLE', 'PULLET', 'DOME');

-- CreateTable
CREATE TABLE "video_devices" (
    "id" TEXT NOT NULL,
    "type" "DeviceType" NOT NULL,
    "serial_number" TEXT NOT NULL,
    "data" JSONB NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL,
    "deleted_at" TIMESTAMPTZ,

    CONSTRAINT "video_devices_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "cameras" (
    "id" TEXT NOT NULL,
    "type" "CameraType" NOT NULL,
    "serial_number" TEXT NOT NULL,
    "data" JSONB NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL,
    "deleted_at" TIMESTAMPTZ,
    "video_device_id" TEXT NOT NULL,

    CONSTRAINT "cameras_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "central_alarms" (
    "id" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "serial_number" TEXT NOT NULL,
    "data" JSONB NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL,
    "deleted_at" TIMESTAMPTZ,

    CONSTRAINT "central_alarms_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "video_devices_serial_number_key" ON "video_devices"("serial_number");

-- CreateIndex
CREATE UNIQUE INDEX "cameras_serial_number_key" ON "cameras"("serial_number");

-- CreateIndex
CREATE UNIQUE INDEX "central_alarms_serial_number_key" ON "central_alarms"("serial_number");

-- AddForeignKey
ALTER TABLE "cameras" ADD CONSTRAINT "cameras_video_device_id_fkey" FOREIGN KEY ("video_device_id") REFERENCES "video_devices"("id") ON DELETE SET NULL ON UPDATE CASCADE;
