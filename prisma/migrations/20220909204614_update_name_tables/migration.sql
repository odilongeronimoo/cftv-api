/*
  Warnings:

  - You are about to drop the `cameras` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `central_alarms` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `video_devices` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "cameras" DROP CONSTRAINT "cameras_video_device_id_fkey";

-- DropTable
DROP TABLE "cameras";

-- DropTable
DROP TABLE "central_alarms";

-- DropTable
DROP TABLE "video_devices";

-- CreateTable
CREATE TABLE "video_device" (
    "id" TEXT NOT NULL,
    "type" "DeviceType" NOT NULL,
    "serial_number" TEXT NOT NULL,
    "data" JSONB NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL,
    "deleted_at" TIMESTAMPTZ,

    CONSTRAINT "video_device_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "camera" (
    "id" TEXT NOT NULL,
    "type" "CameraType" NOT NULL,
    "serial_number" TEXT NOT NULL,
    "data" JSONB NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL,
    "deleted_at" TIMESTAMPTZ,
    "video_device_id" TEXT,

    CONSTRAINT "camera_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "central_alarm" (
    "id" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "serial_number" TEXT NOT NULL,
    "data" JSONB NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL,
    "deleted_at" TIMESTAMPTZ,

    CONSTRAINT "central_alarm_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "video_device_serial_number_key" ON "video_device"("serial_number");

-- CreateIndex
CREATE UNIQUE INDEX "camera_serial_number_key" ON "camera"("serial_number");

-- CreateIndex
CREATE UNIQUE INDEX "central_alarm_serial_number_key" ON "central_alarm"("serial_number");

-- AddForeignKey
ALTER TABLE "camera" ADD CONSTRAINT "camera_video_device_id_fkey" FOREIGN KEY ("video_device_id") REFERENCES "video_device"("id") ON DELETE SET NULL ON UPDATE CASCADE;
