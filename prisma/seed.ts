import { PrismaClient } from '@prisma/client';
import beforeInsertPassword from '../src/shared/utils/before-insert-password.util';
const prisma = new PrismaClient();
const password = beforeInsertPassword('admin');
async function main() {
  const admin = await prisma.user.upsert({
    where: { email: 'admin@gmail.com' },
    update: {},
    create: {
      email: 'admin@gmail.com',
      name: 'ADMIN',
      password,
      roles: { set: ['ADMIN', 'ROOT', 'USER'] },
    },
  });
  console.log(admin);
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
